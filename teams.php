<!DOCTYPE html>
<html>
	<head>
	    <meta charset="UTF-8"> 
		<link rel="stylesheet" href="css/main.css" />
		<link rel="icon" type="image/png" href="img/icon.png" />
		<title>Global Blitz - Týmy</title>
	</head>
    <body>
        <div style="width: 1000px; margin:auto" align="center">
            <h1>Global Blitz BETA - Přehled týmů</h1>
            <p><a href="/"><- Globální Mapa</a></p>
            <?php
				include("credentials.php");
				// Create connection
				$conn = pg_connect($dbstring);
				// Check connection
				if (!$conn)
				{
					echo ('<p>Spojení selhalo</p>');
					return;
				}
				
				$sql = "SELECT * FROM teams";
				$result = pg_query($conn, $sql);

				if (pg_num_rows($result) > 0) { 
                    while($row = pg_fetch_assoc($result))
                    {
                        echo ('<div class="teambox">');
                        echo ("<h3>".$row["name"]."</h3>");
                        echo ('<img src="'.$row["image_url"].'" width="128px" height="128px "/>');
					    echo ('<p class="plist"><b>TAG: </b>' . $row["tag"] . '</p>');
					    echo ('<p class="plist"><b>Barva: </b>'. $row["color"] . "</p>");
					    echo ('<p class="plist"><b>Registrace: </b>'. $row["registered"] . "</p>");
					    echo ('<p class="plist"><b>Vliv: </b>'. $row["poi"] . "</p>");
					    echo ('<p class="plist"><b>Zbývající vliv: </b>'. $row["poi_this_turn"] . "</p>");
					    echo ('<p class="plist"><b>Goldy: </b>'. $row["gold"] . "</p>");
					    echo ('<p class="plist"><b>ELO Hodnocení: </b>'. $row["elo"] . "</p>");
                        echo ('<p class="plist"><b>Ve hře: </b>'. $row["in_game"] . "</p>");
                        echo ('</div>');
                    }
				} else {
    				echo('<h3>Žádné týmy nebyly zaregistrovány</h3>');
				}
				pg_close($conn);
			?>
        </div>
    </body>
</html>
