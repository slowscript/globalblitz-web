<!DOCTYPE html>
<html>
	<head>
	    <meta charset="UTF-8"> 
		<link rel="stylesheet" href="css/main.css" />
		<link rel="icon" type="image/png" href="img/icon.png" />
		<script src="js/p5/p5.js"></script>
		<script src="js/p5/p5.dom.js"></script>
		<script src="js/map.js"></script>
		<title>Global Blitz - Mapa</title>
	</head>
	<body>
		<table align="center" width=1000px>
			<tr> <!-- header -->
				<td valign="top"> <!-- generic website info -->
					<h1>GlobalBlitz BETA - Mapa</h1>
					<p>Vítejte na stránkách turnaje GlobalBlitz - Closed Beta.</p>
					<p>Připojte se na náš Discord server <a href="https://discord.gg/bUzD2q3">zde</a>.</p>
					<p>Kliknutím na Provincii o ní získejte bližší podrobnosti.</p>
					<p><a href="teams.php">Přehled týmů</a></p>
					<p><a href="conflicts.php">Přehled útoků</a></p>
				</td>
				<td> <!-- province info -->
					<iframe id="infobox" src="./province.php?province=A1" width="300px" height="310px" frameBorder="0" ></iframe>
				</td>
			</tr>
		</table>
		<div align="center" width=1000px>
			<!-- map -->
			<div id="mapc">
				<script>
				<?php
					include("credentials.php");
					// Create connection
					$conn = pg_connect($dbstring);
					//Check connection
					if (!$conn)
					{
						echo ("<p>Connection failed: ".$conn->connect_error."</p>");
						return;
					}
					$sql = "SELECT code, team, map_location, status FROM provinces WHERE team!=''";
					$result = pg_query($conn, $sql);

					echo 'var provinces = [';
					if (pg_num_rows($result) > 0) {
						while($row = pg_fetch_assoc($result)) {
							$sql = "SELECT color, image_url FROM teams WHERE tag='" . $row["team"] . "'";
							$result2 = pg_query($conn, $sql);
							$row2 = pg_fetch_assoc($result2);
							echo "['" . $row["map_location"] . "','" . $row2["color"] . "','" . $row2["image_url"] . "','" . $row["code"] . "','" . $row["status"] . "','" . $row["team"] . "'],";
						}
					}
					echo '];';

					$sql = "SELECT province, attacking FROM divisions";
					$divisions = pg_query($conn, $sql);

					echo 'var divisions = [';
					if (pg_num_rows($divisions) > 0) {
						while($row = pg_fetch_assoc($divisions)) {
							echo "['" . $row["province"] . "','" . $row["attacking"] . "'],";
						}
					}
					echo '];';
					pg_close($conn);
				?>
				</script>
			</div>
		</div>
	</body>
</html>
