<!DOCTYPE html>
<html>
	<head>
	    <meta charset="UTF-8"> 
		<link rel="stylesheet" href="css/main.css" />
        <link rel="icon" type="image/png" href="img/icon.png" />
        <script src="js/p5/p5.js"></script>
        <script src="js/p5/p5.dom.js"></script>
		<script src="js/conflicts.js"></script>
		<title>Global Blitz - Útoky</title>
	</head>
    <body>
        <div style="width: 1000px; margin:auto" align="center">
            <h1>Global Blitz BETA - Přehled útoků</h1>
            <p><a href="/">&lt;- Globální Mapa</a></p>
            <?php
				include("credentials.php");
				// Create connection
				$conn = pg_connect($dbstring);
				// Check connection
				if (!$conn)
				{
					echo ('<p>Spojení selhalo</p>');
					return;
				}
                
                // CONFLICTS
				$sql = "SELECT * FROM conflicts WHERE winner is null";
				$result = pg_query($conn, $sql);
                
				if (pg_num_rows($result) > 0) {
                    echo ('<script>var conflicts = [');
                    while($row = pg_fetch_assoc($result))
                    {
                        echo ('[');
                        echo ($row["id"].',');
                        echo ('"'.$row["teams"].'",');
                        echo ('"'.$row["province"].'",');
                        echo ('"'.$row["purpose"].'",');
                        echo ($row["final_battle"]);
                        echo ('],');
                    }
                    echo ('];</script>');
				} else {
                    echo ('<h3>Žádné útoky nenaplánovány</h3>');
                }
                
                //BATTLES
                $sql = "SELECT * FROM battles";
				$result = pg_query($conn, $sql);

				if (pg_num_rows($result) > 0) {
                    echo ('<script>var battles = [');
                    while($row = pg_fetch_assoc($result))
                    {
                        echo ('[');
                        echo ($row["id"].',');
                        echo ('"'.$row["team1"].'",');
                        echo ('"'.$row["team2"].'",');
                        echo ('"'.$row["schedule"].'",');
                        echo ('"'.$row["winner"].'",');
                        echo ('],');
                    }
                    echo ('];</script>');
				} else {
                    echo ('<h3>Žádné bitvy nenalezeny</h3>');
                }

                //TEAMS
                $sql = "SELECT * FROM teams";
				$result = pg_query($conn, $sql);

				if (pg_num_rows($result) > 0) {
                    echo ('<script>var teams = [');
                    while($row = pg_fetch_assoc($result))
                    {
                        echo ('[');
                        echo ('"'.$row["tag"].'",');
                        echo ('"'.$row["image_url"].'"');
                        echo ('],');
                    }
                    echo ('];</script>');
                }

                //OPEN CONFLICTS
                $sql = "SELECT * FROM conflicts WHERE open=b'1'";
				$result = pg_query($conn, $sql);
                
				if (pg_num_rows($result) > 0) {
                    echo ("<h3>Otevřené konflikty:</h3>");
                    while($row = pg_fetch_assoc($result))
                    {
                        echo("<p>".$row["purpose"]." na ".$row["province"].": ".$row["teams"]);
                    }
                }
				pg_close($conn);
			?>
            <div id="cvs" style="position: relative"></div>
        </div>
    </body>
</html>
