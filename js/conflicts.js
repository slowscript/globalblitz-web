/// <reference path="./p5/p5.global-mode.d.ts" />

var w = 1000;
var h = 1000;

function setup()
{
    var canvas = createCanvas(w, h);
    canvas.parent("cvs");
    strokeWeight(3);
    fill(255);
    var x = 0;
    var y = 0;
    for (var i = 0; i < conflicts.length; i++)
    {
        if(conflicts[i][4] == null)
            continue;
        
        textAlign(LEFT, TOP);
        text(conflicts[i][3] + ": " + conflicts[i][2], x+50, y);
        draw_battle(conflicts[i][4], x + 20, y + 25); //Start with final battle

        x += 300;
        //3 Conflicts per line
        if ((i+1) % 3 == 0)
        {
            y += 520;
            x = 0;
        }
    }
}

function draw_battle(id, x, y)
{
    var battle = findBattle(id);
    var point1 = null;
    var point2 = null;
    var maxY;

    //Draw competitors
    if(battle[1].includes("winner")) //Team1 is a winner of another battle -> recursion
    {
        var num = int(battle[1].substr(6));
        data = draw_battle(num,x,y);  
        point1 = data.vector;
        maxY = data.maxY;
    }
    else
    {
        //Draw team1
        var ico = createImg(findIcon(battle[1]), battle[1]);
        ico.parent("cvs");
        ico.position(x-16,y-16);
        ico.size(32, 32);
        point1 = createVector(x,y);
        maxY = y;
    }
    var y2 = maxY + 55;

    if(battle[2].includes("winner")) //Team2 is a winner of another battle -> recursion
    {
        var num = int(battle[2].substr(6));
        data = draw_battle(num,x,y2);
        point2 = data.vector;
        maxY = data.maxY;
    }
    else
    {
        //Draw team2
        var ico = createImg(findIcon(battle[2]), battle[2]);
        ico.parent("cvs");
        ico.position(x-16,y2-16);
        ico.size(32, 32);
        point2 = createVector(x,y2);
        maxY = y2;
    }

    //Draw lines
    x = max(point1.x, point2.x) + 55;
    y = (point1.y + point2.y) / 2;
    line(x, point1.y, x, point2.y);
    
    line(point1.x, point1.y, x, point1.y);
    line(point2.x, point2.y, x, point2.y);

    //Draw text
    textAlign(CENTER, BOTTOM)
    text("ID: " + battle[0], x, y-16);
    var s = battle[3].split(" ");
    textAlign(CENTER, TOP)
    text(s[0], x, y+19);
    text(s[1], x, y+29);
    //Draw winner
    if(battle[4] == "")
        rect(x-16, y-16, 32, 32);
    else
    {
        var ico = createImg(findIcon(battle[4]), battle[4]);
        ico.parent("cvs");
        ico.position(x-16,y-16);
        ico.size(32, 32);
    }
    return {
        vector: createVector(x,y),
        maxY: maxY
    };
}

function findBattle(id)
{
    for(var i = 0; i < battles.length; i++)
    {
        if (battles[i][0] == id)
            return battles[i];
    }
}

function findIcon(team)
{
    for(var i = 0; i < teams.length; i++)
    {
        if (teams[i][0] == team)
            return teams[i][1];
    }
}
