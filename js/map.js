/// <reference path="./p5/p5.global-mode.d.ts" />

var mapSrc = "/img/map.png";
var divIcoSrc = "/img/flag.png";
var auctionIcoSrc = "/img/auction.png";
var attackIcoSrc = "/img/attack.png";
var landingIcoSrc = "/img/landing.png";
var colorNew = [0,255,0,255];
var oldColor;
var img;
var divIco;
var auctionIco;
var attackIco;
var landingIco;
var icoSelected;
var prevIco;
var w = 1025;
var h = 647;

function preload()
{
    img = loadImage(mapSrc);
    divIco = loadImage(divIcoSrc);
    auctionIco = loadImage(auctionIcoSrc);
    attackIco = loadImage(attackIcoSrc);
    landingIco = loadImage(landingIcoSrc);
}

function setup()
{
    var canvas = createCanvas(w, h);
    canvas.parent("mapc");

    strokeWeight(2) 
    img.loadPixels();
    for(var i = 0; i < provinces.length; i++)
    {
        coords = split(provinces[i][0], ','); //province location
        x = int(coords[0]);
        y = int(coords[1]);
        c = color("#"+provinces[i][1]); //team color
        if (provinces[i][4] == "Auction" && provinces[i][5] != "Any") {
            icoURL = "/img/default.png";
            colorNew = [red(c), green(c), blue(c), 180];
        }
        else {
            icoURL = provinces[i][2]; 
            colorNew = [red(c), green(c), blue(c), 255];        
        }
        floodFill(x,y);
        //team img
        var ico = createImg(icoURL);
        ico.parent("mapc");
        ico.position(x-14,y-14);
        ico.size(28, 28);
        ico.html(provinces[i][3]);
        ico.mouseClicked(icoClicked);
    }
    image(img, 0, 0, w, h);
    
    for(var i = 0; i < provinces.length; i++)
    {
        coords = split(provinces[i][0], ','); //province location
        x = int(coords[0]);
        y = int(coords[1]);
        if (provinces[i][4] == "Auction")
            image(auctionIco, x+12, y-12, 24, 24);
        else if (provinces[i][4] == "Attacked")
            image(attackIco, x+12, y-12, 24, 24);
        else if (provinces[i][4] == "Landing")
            image(landingIco, x+12, y-12, 24, 24);
    }
    //Division icons
    for(var i = 0; i < divisions.length; i++)
    {
        var province = findProvince(divisions[i][0]); //Province where division is located
        if(!province)
        {
            console.log("Division on an invalid province!");
            continue;
        }
        coords = split(province[0], ','); //province location
        x = int(coords[0]);
        y = int(coords[1]);
        image(divIco, x-10, y+9, 24, 24);

        var p2 = findProvince(divisions[i][1]); //Province attacked by division
        if(!p2)
            continue;

        coords2 = split(p2[0], ','); //province location
        x2 = int(coords2[0]);
        y2 = int(coords2[1]);
        line(x,y,x2,y2);

        push();
        translate(x2,y2);
        rotate(createVector(x2-x,y2-y).heading());
        triangle(-16,0,-26,-8,-26,8);
        pop();
    }
}

function findProvince(province)
{
    for(var j = 0; j < provinces.length; j++)
        if(provinces[j][3] == province)
            return provinces[j];
}

function mouseClicked()
{
    console.log(mouseX + "," + mouseY); 
}

function icoClicked(evt)
{
    if(icoSelected != null)
    {
        icoSelected.src = prevIco;
    }
    document.getElementById('infobox').src = "./province.php?province=" + evt.target.textContent;
    icoSelected = evt.target;
    prevIco = icoSelected.src;
    icoSelected.src = "img/circle.png";
}

function floodFill(x, y)
{
    oldColor = getPix(x,y);
    //console.log(oldColor);
    //console.log(colorNew);
    if((colorNew[0] == oldColor[0]) && (colorNew[1] == oldColor[1]) && (colorNew[2] == oldColor[2]))
    {
        return;
    }
    console.log("flood start");
    floodFill2(x, y, w, h, true, checkValue, setPix);
    img.updatePixels();
    console.log("flood end");
}

function checkValue(xPos,yPos){    
    var currentPix = getPix(xPos,yPos);
    return ((currentPix[0] == oldColor[0]) && (currentPix[1] == oldColor[1]) && (currentPix[2] == oldColor[2]) && (currentPix[3] == oldColor[3]));
}

function getPix(x,y){
    var color = [];

    idx = 4 * (y * width + x);
    color[0] = img.pixels[idx];
    color[1] = img.pixels[idx+1];
    color[2] = img.pixels[idx+2];
    color[3] = img.pixels[idx+3];
  
    return color;
}
  
function setPix(x, y) {
    idx = 4 * (y * width + x);
    img.pixels[idx] = colorNew[0];
    img.pixels[idx+1] = colorNew[1];
    img.pixels[idx+2] = colorNew[2];
    img.pixels[idx+3] = colorNew[3];
}

function floodFill2(x, y, width, height, diagonal, test, paint) {
    // xMin, xMax, y, down[true] / up[false], extendLeft, extendRight
    var ranges = [[x, x, y, null, true, true]];
    paint(x, y);

    while(ranges.length) {
        var r = ranges.pop();
        var down = r[3] === true;
        var up =   r[3] === false;

        // extendLeft
        var minX = r[0];
        var y = r[2];
        if(r[4]) {
            while(minX>0 && test(minX-1, y)) {
                minX--;
                paint(minX, y);
            }
        }
        var maxX = r[1];
        // extendRight
        if(r[5]) {
            while(maxX<width-1 && test(maxX+1, y)) {
                maxX++;
                paint(maxX, y);
            }
        }

        if(diagonal) {
            // extend range looked at for next lines
            if(minX>0) minX--;
            if(maxX<width-1) maxX++;
        }
        else {
            // extend range ignored from previous line
            r[0]--;
            r[1]++;
        }

        function addNextLine(newY, isNext, downwards) {
            var rMinX = minX;
            var inRange = false;
            for(var x=minX; x<=maxX; x++) {
                // skip testing, if testing previous line within previous range
                var empty = (isNext || (x<r[0] || x>r[1])) && test(x, newY);
                if(!inRange && empty) {
                    rMinX = x;
                    inRange = true;
                }
                else if(inRange && !empty) {
                    ranges.push([rMinX, x-1, newY, downwards, rMinX==minX, false]);
                    inRange = false;
                }
                if(inRange) {
                    paint(x, newY);
                }
                // skip
                if(!isNext && x==r[0]) {
                    x = r[1];
                }
            }
            if(inRange) {
                ranges.push([rMinX, x-1, newY, downwards, rMinX==minX, true]);
            }
        }

        if(y<height)
            addNextLine(y+1, !up, true);
        if(y>0)
            addNextLine(y-1, !down, false);
    }
}
