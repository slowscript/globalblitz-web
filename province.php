<html>
	<head>
		<meta charset="UTF-8"> 
		<link rel="stylesheet" href="css/main.css" />
		<title>Province Info Box</title>
	</head>
	<body class="nobg"> 
		<div class="box">
			<h2>Province</h2>
			<?php				
				include("credentials.php");
				// Create connection
				$conn = pg_connect($dbstring);
				// Check connection
				if (!$conn)
				{
					echo ('<p>Spojení selhalo</p>');
					return;
				}
				$province = htmlspecialchars($_GET["province"]);
				//"Escape string so it is safe to use in a query"
				$province = pg_escape_string($conn, $province);
				
				$sql = "SELECT * FROM provinces WHERE code='$province'";
				$result = pg_query($conn, $sql);

				if (pg_num_rows($result) > 0) {
					// output data of a row
 					$row = pg_fetch_assoc($result);
					echo ("<h3>".$row["name"]."</h3>");
					echo ('<p class="plist"><b>KÓD: </b>' . $province . '</p>');
					if ($row["status"] != 'Auction') {
					echo ('<p class="plist"><b>Tým: </b>'. $row["team"] . "</p>");
					}
					echo ('<p class="plist"><b>Mapa: </b>'. $row["map"] . "</p>");
					echo ('<p class="plist"><b>Hranice: </b>'. $row["borders"] . "</p>");
					echo ('<p class="plist"><b>Stav: </b>'. $row["status"] . "</p>");
					//echo ('<p class="plist"><b>Level: </b>'. $row["level"] . "</p>");
					//echo ('<p class="plist"><b>Eco: </b>'. $row["eco"] . "</p>");
					//echo ('<p class="plist"><b>Revolt: </b>'. $row["revolt"] . "</p>");
					if ($row["status"] == 'Auction') {
						echo ('<p class="plist"><b>Nejvyšší nabídka: </b>'. $row["highest_bid"] . "</p>");
						echo ('<p class="plist"><b>Nejvyšší nabídka od: </b>'. $row["team"] . "</p>");
					}
				} else {
    				echo('Provincie "'.$province.'" nenalezena');
				}
				pg_close($conn);
			?>
		</div>
	</body>
</html>
